FROM centos/python-38-centos7

WORKDIR /app
COPY . /app
RUN pip install -r requirements.txt

ENV PYTHONPATH=/app
CMD chmod 777 -R /app
CMD pytest -n=4 --alluredir=./allure-results test
